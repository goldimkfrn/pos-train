
    @extends('layouts.app')

    @section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if(Auth::user()->role == '1')
                        <div class="card-header">Welcome ADMIN!</div>

                        <div class="card-body">
                            Hello {{ Auth::user()->name }} <br/>
                            Email anda : {{ Auth::user()->email }} <br/>
                            Anda login menggunakan username : Supervisor <br/>
                        </div>
                    </div>

                @else
                    <form method="POST" name="form-transaction" action="{{  URL('/inserttransacsaction') }}">
                        <div class="card-header">Welcome, Cashier {{ Auth::user()->name }} </div>
                        <br/>

                        <div class="form-group row" style="margin-top: 20px;">
                                <label for="customer" class="col-md-4 col-form-label text-md-right">{{ __('Customer Name') }}</label>

                                <div class="col-md-6">
                                    <input id="customer" type="text" class="form-control @error('name') is-invalid @enderror" name="customer" value="{{ old('customer') }}" required autocomplete="customer" autofocus>

                                    @error('customer')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <input type="number" name="tax" value="1" hidden>

                        <div id="insert" class="insert">

                            {{ csrf_field() }}


                        </div>
                        <button type="submit" class="btn btn-primary" style="margin-left: 600px; margin-bottom: 10px;"  >Submit</button>
                    </form>
                @endif

            </div>

        </div>

    </div>

    <script type="text/javascript">

        $(document).ready(function(){

            dataitem();
            select2();

        });

        function dataitem()
        {

            var html = '';


            // Input data Item
            html = '<div class="form-group row {{ $errors->has('item_id') ? ' has-error' : '' }}">';
                html += '<label class="col-md-4 control-label text-md-right">Item</label>';
                html += '<div class="col-md-6">';
                    html += '<select name="item_id" id="item_id" onkeyup="select2()" class="form-control item_id">';
                        html += '<option></option>';
                        html += ' @foreach ($price as $i)';
                            html += '<option item="{{  $i->item_name }}" item_id="{{  $i->id }}" id_unit="{{  $i->unit_id }}" sku="{{  $i->sku }}" unit="{{  $i->unit }}" price="{{  $i->price }}" value="{{  $i->id }}"><?php echo $i->item_name, " - ", $i->unit, " - ", $i->price; ?></option>';
                            html += '@endforeach';
                    html += '</select>';
                html += '</div>';
            html += '</div>';


            // Input data qty
            html += '<div class="form-group row" style="margin-top: 20px;">';
                html += '<label for="qty" class="col-md-4 col-form-label text-md-right">{{ __('Qty') }}</label>';
                html += '<div class="col-sm-2">';
                    html += '<input id="qty" type="number" maxlength="1"  class="form-control" name="qty[]" value="{{ old('qty') }}" required autocomplete="qty" autofocus>';
                html += '</div>';
            html += '</div>';


            // Add data
            html += '<div class="form-group row mb-0">';
                html += '<div class="col-md-6 offset-md-4">';
                    html += '<span onclick="addDetail()" id="add" class="btn btn-primary save">';
                        html += '{{ __('+') }}';
                    html += '</span>';
                html += '</div>';
            html += '</div>';
            



            html += '<br/>';
            html += '</div>';
            html += '<div class="card-body">';


                //Table
                html += '<table class="table table-sm">';
                    html += '<thead>';
                        html += '<tr>';
                            html += '<th scope="col">No</th>';
                            html += '<th id="item_name" scope="col">Item Name</th>';
                            html += '<th id="sku" scope="col">SKU</th>';
                            html += '<th id="unit" scope="col">Unit</th>';
                            html += '<th id="qty" scope="col">Qty</th>';
                            html += '<th id="price" scope="col">Price</th>';
                            html += '<th scope="col"></th>';
                        html += '</tr>';
                    html += '</thead>';

                    html += '<tbody id="detailData">';
                    html += '</tbody>';
                html += '</table>';
                html += '<table>';
                    html += '<td>';
                    html += '<h5>Total : <span id="totalCap"></span></h5>';
                    html += '</td>';
                html += '</table>';
                html += '<input type="number" name="totala" id="totala" hidden/>';
                html += "<input type='text' name='user_id' value='{{  Auth::user()->id }}' hidden >";

            
            $('#insert').append(html);

           
        }

        function total(){
            var sumTotal = 0;
            $('.subTotal').each(function(){
                sumTotal += parseFloat(this.value);
            });

            $('#totala').val(sumTotal);

            return sumTotal;


        }

        var noItem = 0;

        function addDetail(){


            var quantity = $('#qty').val();
            var prices = $("#item_id option:selected").attr("price");
            var subtotal = parseInt(quantity)*parseInt(prices);
            noItem++;

            var data = "";

                data += "<tr id='Courses'>";
                data += "<td>";
                data += noItem; 
                data += "</td>";
                data += "<td>";
                data += $("#item_id option:selected").attr("item");
                data += "</td>";
                data += "<td>";
                data += $("#item_id option:selected").attr("sku");
                data += "</td>";
                data += "<td>";
                data += $("#item_id option:selected").attr("unit");
                data += "</td>";
                data += "<td>";
                data += $('#qty').val();
                data += "</td>";
                data += "<td>";
                data += subtotal;
                data += "<input id='qty' type='number' name='detail["+noItem+"][qty]' value='"+$('#qty').val()+"' hidden>";
                data += "<input type='text' name='detail["+noItem+"][item_id]' value='"+$("#item_id option:selected").attr("item_id")+"' hidden> ";
                data += "<input type='text' name='detail["+noItem+"][unit_id]' value='"+$("#item_id option:selected").attr("id_unit")+"' hidden> ";
                data += "<input type='text' name='detail["+noItem+"][price]' value='"+$("#item_id option:selected").attr("price")+"' hidden> ";
                data += "<input tupe='text' id='subtotal' name='detail["+noItem+"][subtotal]' class='subTotal' value='"+subtotal+"' hidden > ";
                data += "<input type='number' name='detail["+noItem+"][disc]' value='0' hidden>";
                data += "</td>";
                data += "<td>";
                data += '<button type="button" onclick="delete()" id="delete" class="btn btn-danger "> {{ __("delete") }} </button> ';
                data += "</td>";
                data += "</tr>";


            $("#detailData").append(data);

            $("#totalCap").html(total());

        }

       function select2()
       {     
        $(".item_id").select2({
            placeholder: "Select an Item",
            allowClear: true
            });
        } 

    </script>


@endsection

