
    @extends('layouts.app')

    @section('content')	
    <div class="container"> 
      <a href="{{  URL('/showtransaction') }}" class="btn btn-secondary">Back</a>
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3><?php echo $details->customer_name; ?></h3></div>
                
                

                <div class="card-body">			
        <div class="card-body">
                    <table class="table table-sm">
                      <thead>
                        <tr>

                          <th scope="col">No</th>
                          <th scope="col">Item Name</th>
                          <th scope="col">Unit</th>
                          <th scope="col">Price</th>
                          <th scope="col">Qty</th>
                          <th scope="col">Subtotal</th>
                        </tr>
                      </thead>

                      @php

                      $no=1;
                      $tot=0;

                      @endphp

                      @foreach($details->Detail as $i)
                      
                      <tbody>
                        <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{  $i->item_name }}</td>
                        <td>{{  $i->unit }}</td>
                        <td>{{  $i->price }}</td>
                        <td>{{  $i->qty }}</td>
                        <td>{{  $i->subtotal }}</td>
                        @php
                        $tot = $tot + $i->subtotal;
                        @endphp

                        
                      </tbody>
                      @endforeach
                    </table>
                    <h5>Total : <?php echo $tot; ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection