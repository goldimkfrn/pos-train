<form method="POST" action="/home">
                        <div class="card-header">Welcome, Cashier {{ Auth::user()->name }} </div>
                        <br/>

                            

                            <div class="form-group row {{ $errors->has('item_id') ? ' has-error' : '' }}">
                               <label class="col-md-4 control-label text-md-right">Item Name</label>                                
                                <div class="col-md-6">                                    
                                   <select name="item_id" class="form-control">
                                        @foreach ($item as $i)
                                       <option value="{{  $i->id }}">{{  $i->item_name }}</option>
                                       @endforeach
                                   </select>
                                </div>                               
                            </div>

                            <div class="form-group row {{ $errors->has('unit_id') ? ' has-error' : '' }}">
                               <label class="col-md-4 control-label text-md-right">Unit</label>                                
                                <div class="col-md-6">                                    
                                   <select name="unit_id" class="form-control">
                                        @foreach ($unit as $u)
                                       <option value="{{  $u->id }}">{{  $u->unit }}</option>
                                       @endforeach
                                   </select>
                                </div>                               
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                        </div>
                    <br/>
                    </div>
                </form>

                <table class="table table-sm">

                    <div class="form-group row" style="margin-top: 20px;">
                                <label for="customer" class="col-md-4 col-form-label text-md-right">{{ __('Customer Name') }}</label>

                                <div class="col-md-6">
                                    <input id="customer" type="text" class="form-control @error('name') is-invalid @enderror" name="customer" value="{{ old('customer') }}" required autocomplete="customer" autofocus>

                                    @error('customer')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                      <thead>
                        <tr>

                          <th scope="col">No</th>
                          <th scope="col">Item Name</th>
                          <th scope="col">Unit</th>
                          <th scope="col">Qty</th>
                          <th scope="col">Price</th>
                          <th scope="col">Sub Total</th>
                          <th></th>
                        </tr>
                      </thead>
                      
                      <tbody>
                        
                        <tr>

                        </tr>
                        
                      </tbody>
                    </table>
