
    @extends('layouts.app')

    @section('content')	
    <div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Transaction') }}</div>
                
                

                <div class="card-body">			
        <div class="card-body">
                    <table class="table table-sm">
                      <thead>
                        <tr>

                          <th scope="col">No</th>
                          <th scope="col">Tanggal</th>
                          <th scope="col">Customer</th>
                          <th scope="col">Cashier</th>
                          <th scope="col">Total</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>

                      @php 

                      $no=1;
                      $tot=0;

                      @endphp

                      @foreach($transaction as $i)
                      
                      <tbody>
                        <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{  $i->updated_at }}</td>
                        <td>{{  $i->customer_name }}</td>
                        <td>{{  $i->user_name }}</td>
                        <td>{{  $i->total }}</td>
                        @php
                        $tot = $tot + $i->total;
                        @endphp
                        <td><a href="/showdetail/{{  $i->id }}" type="button" class="btn btn-success btn-sm">Lihat</a> </td>

                        
                      </tbody>
                      @endforeach

                    </table>
                    <h5 style="margin-top: 100px;">Total : <?php echo $tot; ?></h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection