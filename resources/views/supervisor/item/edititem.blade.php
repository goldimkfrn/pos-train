@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit User') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ URL('updateitem', $update -> id) }}">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="item_name" class="col-md-4 col-form-label text-md-right">{{ __('Item Name') }}</label>

                            <div class="col-md-6">
                                <input id="item_name" type="text" class="form-control @error('item_name') is-invalid @enderror" name="item_name" value="{{ $update->item_name }}" required autocomplete="item_name" autofocus>

                                @error('item_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                            <div class="form-group row {{ $errors->has('username') ? ' has-error' : '' }}">
                               <label class="col-md-4 control-label text-md-right">Username</label>

                                <div class="col-md-6">
                                   <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $update->username }}" required autocomplete="username" autofocus>

                                   @if ($errors->has('username'))
                                      <span class="help-block">
                                         <strong>{{ $errors->first('username') }}</strong>
                                      </span>
                                   @endif
                                </div>
                            </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
