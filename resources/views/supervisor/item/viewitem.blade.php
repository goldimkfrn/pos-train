@extends('layouts.app')

@section('content')

<div class="container">
  <a href="{{  URL('items') }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="Tooltip on left">
      New Item
  </a>
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Item') }}</div>
                
                

                <div class="card-body">
                    <table class="table table-sm">
                      <thead>
                        <tr>

                          <th scope="col">No</th>
                          <th scope="col">Item Name</th>
                          <th scope="col">SKU</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>

                      @php $no=1;
                      @endphp

                      @foreach($item as $i)
                      
                      <tbody>
                        <tr>
                         
                         <td>{{ $no++ }}</td>
                          <td>{{  $i->item_name }}</td>
                          <td>{{  $i->sku }}</td>
                          <td><a href="/edititem/{{ $i->id }}" type="button" class="btn btn-success btn-sm">Edit</a> 
                            <a href="/deleteitem/{{ $i->id }}" type="button" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                        </tr>
                        
                      </tbody>
                      @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

