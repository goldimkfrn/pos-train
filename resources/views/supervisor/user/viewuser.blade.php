@extends('layouts.app')

@section('content')

<div class="container">
  <a href="{{  URL('newuser') }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="Tooltip on left">
      New User
  </a>
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Tabel User') }}</div>
                
                

                <div class="card-body">
                    <table class="table table-sm">
                      <thead>
                        <tr>

                          <th scope="col">No</th>
                          <th scope="col">Username</th>
                          <th scope="col">Full Name</th>
                          <th scope="col">Role</th>
                          <th scope="col">Email</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      @php $no=1;
                      @endphp
                      @foreach($users as $u)
                      
                      <tbody>
                        
                        <tr>
                          
                          <td>{{  $no++ }}</td>
                          <td>{{  $u->username }}</td>
                          <td>{{  $u->name }}</td>
                          <td>{{  $u->email }}</td>
                          <td>@if ($u->role == '1') <?php echo "Supervisor"; ?>
                              @else <?php echo "Cashier"; ?>
                              @endif
                          </td>
                          <td><a href="/edituser/{{ $u->id }}" type="button" class="btn btn-success btn-sm">Edit</a></td>
                          <td><a href="/deleteuser/{{ $u->id  }}" type="button" class="btn btn-danger btn-sm">Delete</a></td>

                        </tr>
                        
                      </tbody>
                      @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

