@extends('layouts.app')

@section('content')

<div class="container">
  <a href="{{  URL('units') }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="left" title="Tooltip on left">
      New Unit
  </a>
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('unit') }}</div>
                
                

                <div class="card-body">
                    <table class="table table-sm">
                      <thead>
                        <tr>

                          <th scope="col">No</th>
                          <th scope="col">unit Name</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>

                      @php $no=1;
                      @endphp

                      @foreach($unit as $i)
                      
                      <tbody>
                        <tr>
                         
                         <td>{{ $no++ }}</td>
                          <td>{{  $i->unit }}</td>
                          <td> 
                            <a href="/deleteunit/{{ $i->id }}" type="button" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                        </tr>
                        
                      </tbody>
                      @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

