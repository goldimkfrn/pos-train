@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
              <form method="POST" action="{{ URL('insertprice') }}" style="margin-top: 50px; margin-bottom: 50px;">
                        @csrf

                            
                            <div class="form-group row {{ $errors->has('item_id') ? ' has-error' : '' }}">
                               <label class="col-md-4 control-label text-md-right">Item Name</label>                                
                                <div class="col-md-6">                                    
                                   <select name="item_id" class="form-control">
                                        @foreach ($item as $d)
                                       <option value="{{  $d->id }}">{{  $d->item_name }}</option>
                                       @endforeach
                                   </select>
                                </div>                               
                            </div>

                            <div class="form-group row {{ $errors->has('unit_id') ? ' has-error' : '' }}">
                               <label class="col-md-4 control-label text-md-right">Unit</label>                                
                                <div class="col-md-6">                                    
                                   <select name="unit_id" class="form-control">
                                        @foreach ($unit as $d)
                                       <option value="{{  $d->id }}">{{  $d->unit }}</option>
                                       @endforeach
                                   </select>
                                </div>                               
                            </div>
                             

                            <div class="form-group row {{ $errors->has('price') ? ' has-error' : '' }}">
                               <label class="col-md-4 control-label text-md-right">prices</label>

                                <div class="col-md-6">
                                   <input type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}" required autocomplete="price" autofocus>

                                   @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                <div class="card-header">{{ __('Tabel Data') }}</div>


                
                

                <div class="card-body">

                    <table class="table table-sm">
                      <thead>
                        <tr>

                          <th scope="col">No</th>
                          <th scope="col">Item Name</th>
                          <th scope="col">SKU</th>
                          <th scope="col">Unit</th>
                          <th scope="col">Price</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>

                      @php $no=1;
                      @endphp

                      @foreach($price as $d)
                      
                      <tbody>
                        <tr>
                         
                         <td>{{ $no++ }}</td>
                          <td>{{  $d->item_name }}</td>
                          <td>{{  $d->sku }}</td>
                          <td>{{  $d->unit }}</td>
                          <td>{{  $d->price }}</td>
                          <td><a href="/deletedata/{{ $d->id  }}" type="button" class="btn btn-danger btn-sm">Delete</a></td>
                        </tr>
                        
                      </tbody>
                      @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

