<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'role=>supervisor'], function()
{
Route::match(['get', 'post'], '/isi', 'HomeController@laporan');
});


Route::group(['middleware' => 'role=>kasir'], function()
{
Route::match(['get', 'post'], '/home', 'HomeController@home');
});

Route::get('/home', 'HomeController@index')->name('home');

//Transaction


// USER
Route::get('/newuser', 'HomeController@newuser');
Route::post('/insertuser', 'HomeController@insertuser');
Route::get('/viewuser', 'HomeController@viewuser');
Route::get('/edituser/{id}', 'HomeController@edituser');
Route::post('/updateuser/{id}', 'HomeController@updateuser');
Route::get('/deleteuser/{id}', 'HomeController@deleteuser');

// Item
Route::get('/items', 'ItemController@item');
Route::post('/insertitem', 'ItemController@insertitem');
Route::get('/viewitem', 'ItemController@viewitem');
Route::get('/edititem/{id}', 'ItemController@edititem');
Route::get('/updateitem/{id}', 'ItemController@edititem');
Route::get('/deleteitem/{id}', 'ItemController@deleteitem');

// Unit
Route::get('/units', 'UnitController@unit');
Route::post('/insertunit', 'UnitController@insertunit');
Route::get('/viewunit', 'UnitController@viewunit');
Route::get('/delete/{id}', 'UnitController@deleteunit');


// Price
Route::get('/price', 'PriceController@price');
Route::post('/insertprice', 'PriceController@insertprice');

//Data
Route::get('/viewdata', 'PriceController@viewdata');
Route::get('/editdata/{id}', 'PriceController@editdata');
Route::get('/deletedata/{id}','PriceController@deletedata');

//Transaction
Route::post('/inserttransacsaction', 'DetailController@insert');
Route::get('/showtransaction', 'TransactionController@index');
Route::get('/showdetail/{id}', 'TransactionController@detail');





