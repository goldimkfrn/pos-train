<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    	'id',
    	'user_id',
    	'customer_name',
    	'total',
    	'tax',


    ];

    public function Detail(){

        return $this->hasMany('App\Detail')->join('items', 'items.id', '=', 'transaction_details.item_id')
                                           ->join('units', 'units.id', '=', 'transaction_details.unit_id');

    }
}
