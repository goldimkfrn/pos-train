<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Item;
use App\User;
use App\Detail;
use App\Unit;
use App\Price;

class HomeController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        
        $price = Price::join('items', 'items.id', '=', 'prices.item_id')
                                    ->join('units', 'units.id', '=', 'prices.unit_id')
                                    ->select(
                                             'items.id as id',
                                             'prices.unit_id as unit_id',
                                             'units.unit as unit',
                                             'items.item_name as item_name',
                                             'items.sku as sku',
                                             'prices.price as price',
                                             'prices.id as price_id',
                                             )
                                    ->get();

        return view('home')->with('price', $price);

    }

     //USER
    public function viewuser()
    {
        
        $users = DB::table('users')->get();

        return view('supervisor/user/viewuser',['users' => $users]);
    }


    public function newuser()
    {
        return view('supervisor/user/user');
    }

    public function insertuser(Request $data)
    {
        DB::table('users')->insert([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => Hash::make($data['password'])
        ]);

    return redirect('/viewuser');

    }

    public function edituser($id)
    {

        $update = User::where('id',$id)->first();
        return view('supervisor/user/edituser')->with('update', $update);
    }

    public function updateuser(Request $Request, $id)
    {
        $update = User::where('id', $id)->first();
        $update->name = $Request['name'];
        $update->username = $Request['username'];
        $update->email = $Request['email'];
        $update->role = $Request['role'];
        $update->password = Hash::make($Request['password']);
        $update->update();

        return redirect()->to('/viewuser');
    }

    public function deleteuser($id)
    {
        $delete = User::find($id);
        $delete->delete();

        return redirect()->to('/viewuser');
    }

}
