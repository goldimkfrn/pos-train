<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Unit;

class UnitController extends Controller
{
    //ITEMS
    public function unit()
    {
        // dd("asd");
        return view('supervisor/unit/unit');
    }

    public function insertunit(Request $data)
    {

    DB::table('units')->insert([

    'unit' => $data->unit,
    
    ]);
 
    return redirect('/viewunit');

    }

    public function viewunit()
    {
        $unit = DB::table('units')->get();
        return view('supervisor/unit/viewunit', ['unit' => $unit]);
    }

    public function deleteunit($id)
    {
        $delete = Unit::find($id);
        $delete->delete();

        return redirect()->to('/viewunit');
    }
}
