<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Detail;
use App\Transaction;
use App\User;

class DetailController extends Controller
{
    public function index()
    {
        $item = DB::table('items')->get();
        $unit = DB::table('units')->get();
        $price = DB::table('prices')->get();
        $transaction = DB::table('transactions')->get();
        $detail = DB::table('transaction_details')->get();
        return view('viewdata')->with('item', $item)
	                           ->with('unit', $unit)
	                           ->with('price', $price)
	                           ->with('transaction', $transaction)
	                           ->with('detail', $detail);
    }

    public function insert(Request $data)
    {


    	$id = Transaction::max('id');
    	
    	$transaction_id = $id++;

    	$head = [

    	'customer_name' => $data->customer,
    	'total' => $data->totala,
    	'user_id' => $data->user_id,
    	'tax' => $data->tax

    	];

    	$transaction = Transaction::create($head);

    	// //array detail,menggunakan id transaction yg sudah dibua tadi		


    	$detail = $data->detail;
    	foreach ($detail as $key => $value) {

    	$detail = [

    		'transaction_id' => $transaction->id,
    		'item_id' => $value['item_id'],
    		'unit_id' => $value['unit_id'],
    		'price' => $value['price'],
    		'qty' => $value['qty'],
    		'subtotal' => $value['subtotal'],
    		'disc' => $value['disc']

    	];


    	Detail::create($detail);

    	}

    	return redirect ('/home');


    	// //insert detail menggunaan array detail 


    }



}
