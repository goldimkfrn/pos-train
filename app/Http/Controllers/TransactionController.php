<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Detail;
use App\Transaction;
use App\User;

class TransactionController extends Controller
{
    public function index(){

   	 	$transaction = Transaction::join('users', 'users.id', '=', 'transactions.user_id')
   	 							   ->select(

   	 							   		'transactions.id as id',
   	 							   		'transactions.customer_name as customer_name',
   	 							   		'users.name as user_name',
   	 							   		'transactions.total as total',
   	 							   		'transactions.updated_at as updated_at',

   	 									   )
   	 							   ->get();
   	 	// dd($transaction);
    	return view('show')->with('transaction', $transaction);	
    }

    public function detail($id){

    	$details = Transaction::with('Detail')->find($id);

    	return view('showdetail')->with('details', $details);


    }
}
