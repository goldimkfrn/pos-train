<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Price;

class PriceController extends Controller
{

	 public function price()
    {
        $item = DB::table('items')->get();
        $unit = DB::table('units')->get();
        $price = DB::table('prices')->get();
        return view('viewdata')->with('item', $item)
                           ->with('unit', $unit)
                           ->with('price', $price);
    }

    public function insertprice(Request $data3)

    {


        DB::table('prices')
                           ->insert([ 

                            'item_id' => $data3->item_id,
                            'unit_id' => $data3->unit_id,
                            'price' => $data3->price,
                            
                            ]);

        return redirect ('/viewdata');

    }

    public function viewdata()
    {
        $item = DB::table('items')->get();
        $unit = DB::table('units')->get();                                               
        $price = DB::table('prices')->join('items', 'items.id', '=', 'prices.item_id')
                                    ->join('units', 'units.id', '=', 'prices.unit_id')
                                    ->select(
                                             'prices.id as id',
                                             'items.item_name as item_name',
                                             'items.sku as sku',
                                             'prices.price as price',
                                             'units.unit as unit',
                                            )
                                    ->get();


        return view('supervisor/price/viewprice',)->with('item', $item)
                                                  ->with('unit', $unit)
                                                  ->with('price', $price);
    }

    public function deletedata($id)
    {
        $delete = Price::find($id);
        $delete->delete();

        return redirect()->to('/viewdata');
    }

}
