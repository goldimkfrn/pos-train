<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Item;

class ItemController extends Controller
{
    //ITEMS
    public function item()
    {
        // dd("asd");
        return view('supervisor/item/item');
    }

    public function insertitem(Request $data)
    {

    DB::table('items')->insert([

    'item_name' => $data->item_name,

    'SKU' => $data->sku,
    
    ]);
 
    return redirect('/viewitem');

    }

    public function viewitem()
    {
        $item = DB::table('items')->get();
        return view('supervisor/item/viewitem', ['item' => $item]);
    }

    public function edititem($id)
    {

        $update = Item::where('id',$id)->first();
        return view('supervisor/item/edititem')->with('update', $update);
    }

    public function updateitem(Request $Request, $id)
    {
        $update = Item::where('id', $id)->first();
        $update->item_name = $Request['item_name'];
        $update->sku = $Request['sku'];
        $update->update();

        return redirect()->to('/viewitem');
    }

    public function deleteitem($id)
    {
        $delete = Item::find($id);
        $delete->delete();

        return redirect()->to('/viewitem');
    }
}
