<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'transaction_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id',
    	'transaction_id',
    	'item_id',
        'unit_id',
    	'price',
        'qty',
        'subtotal',
        'disc'
    ];

    public function Transaction(){

        return $this->belongsTo('App\Transaction');

    }
}
